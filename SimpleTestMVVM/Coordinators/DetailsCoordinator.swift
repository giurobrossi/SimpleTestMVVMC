//
//  DetailsCoordinator.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 24/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import UIKit

struct DetailsCoordinator: Coordinator {
    
    //var window: UIWindow
    var navController: UINavigationController
    
    private var user: User
    
    init(navController: UINavigationController, withData data: User) {
        self.navController = navController
        self.user = data
    }
    
    func start() {
        // Storyboard instance
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        // ViewController instance
        if let vc = storyboard.instantiateViewController(withIdentifier: "DetailsVC") as? DetailsVC {
            
            // ViewModel instance
            var viewModel = DetailsVM(coordinator: self)
            
            // Pass data (coming from SearchCoordinator) to DetailsVM that will be injected into DetailsVC. 
            viewModel.user = self.user
            
            // Injects ViewModel in the ViewController by vc.viewModel property. Property's type is a protocol (SearchVMProtocol).
            vc.viewModel = viewModel
            
            // Set SearcVC as the main VC
            navController.pushViewController(vc, animated: true)
        }
    }
    
    func transition(toScreen screen: Screen, withData data: Any?) {
        if(screen == Screen.main) {
            // Instantiate SearchCoordinator and injects the window object.
            let searchCoordinator = SearchCoordinator(navController: self.navController)
            // triggers start() method
            searchCoordinator.start()
        }
        else {
            print("The destination of transition is not correct: \(screen)")
        }
    }
}
