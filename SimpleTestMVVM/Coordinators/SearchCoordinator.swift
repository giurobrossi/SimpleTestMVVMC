//
//  SearchCoordinator.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 23/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import UIKit

struct SearchCoordinator: Coordinator {
    
    //var window: UIWindow
    let navController: UINavigationController
    
    init(navController: UINavigationController) {
        self.navController = navController
    }
    
    func start() {
        // Storyboard instance
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        // ViewController instance
        if let vc = storyboard.instantiateViewController(withIdentifier: "SearchVC") as? SearchVC {
            // ViewModel instance
            let viewModel = SearchVM(coordinator: self)
            // Injects ViewModel in the ViewController by vc.viewModel property. Property's type is a protocol (SearchVMProtocol).
            vc.viewModel = viewModel
            // Set SearcVC as the main VC
            navController.setViewControllers([vc], animated: true)
        }
    }
    
    func transition(toScreen screen: Screen, withData data: Any?) {
        if(screen == Screen.detail) {
            // Instantiate SearchCoordinator and injects the window object.
            let detailsCoordinator = DetailsCoordinator(navController: navController, withData: (data as! User) )
            // triggers start() method
            detailsCoordinator.start()
        }
        else {
            print("The destination of transition is not correct: \(screen)")
        }
    }
}
