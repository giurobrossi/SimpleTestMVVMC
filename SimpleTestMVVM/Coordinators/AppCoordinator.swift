//
//  AppCoordinator.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 22/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import UIKit

struct AppCoordinator: Coordinator {
    
    let window: UIWindow

    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        // Prepare NavigationController
        let navController = UINavigationController()
        navController.navigationBar.barStyle = .black
        
        // Instantiate SearchCoordinator and injects the window object.
        //let searchCoordinator = SearchCoordinator(window: window)
        let searchCoordinator = SearchCoordinator(navController: navController)
        // triggers start() method
        searchCoordinator.start()
        
        window.rootViewController = navController
    }
    
}
