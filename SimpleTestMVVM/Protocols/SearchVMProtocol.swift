//
//  SearchViewModelProtocol.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 23/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import Foundation

protocol SearchVMProtocol {
    
    func searchUser(userName: String)
    
    // Pass Back delegate searchVMDelegate has been declared here in the SearchVMProtocol, because SearchVC.viewDelegate is a SearchVMProtocol type and the variable must be defined also inside here (not only in the SearchVM struct). Moreover in this way I setup as standard practice to define a passback delegate from VM to VC.
    var searchVMDelegate: SearchVMDelegate? {get set}
    
}

// Default behaviour in order to: debug/mockup and making the method implementation not manadatory in the class/struct
extension SearchVMProtocol {
    
    func searchUser(userName: String) {
        print("searchUser invoked: \(userName)")
    }
}
