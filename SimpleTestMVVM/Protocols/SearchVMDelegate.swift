//
//  SearchVMDelegate.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 23/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import Foundation

protocol SearchVMDelegate {
    
    func feedback(data: String)
}

// Default behaviour in order to: debug/mockup and making the method implementation not manadatory in the class/struct
extension SearchVMDelegate {
    
    func feedback(data: String) {
        print("User Not Found")
    }
}

