//
//  DetailsVMProtocol.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 24/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import Foundation

protocol DetailsVMProtocol {
 
    var detailsVMDelegate: DetailsVMDelegate? {get set}
    
    var user: User? {get set}
    
    func didSave(user: User)
    
    func didCancel()
}

extension DetailsVMProtocol {
    
    func didSave(user: User) {
        print("Update Model and Back to SearchVC: \(user)")
    }
    
    func didCancel() {
        print("Back to SearchVC")
    }
}
