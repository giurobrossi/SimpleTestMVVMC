//
//  Coordinator.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 22/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

protocol Coordinator {
    
    func start()
    
    //func transition(user: User?)
    
    func transition(toScreen screen: Screen, withData data: Any?)
}


// Default behaviour in order to: debug/mockup and making the method implementation not manadatory in the class/struct
extension Coordinator {
    
//    func transition(user: User?) {
//        print("GO")
//    }
    
    func transition(toScreen screen: Screen, withData data: Any?) {
        print("transition to: \(screen)")
    }
}
