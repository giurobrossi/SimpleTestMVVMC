//
//  DetailsVMDelegate.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 24/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import Foundation

protocol DetailsVMDelegate: class {
    
    func feedback(data: String)
}

// Default behaviour in order to: debug/mockup and making the method implementation not manadatory in the class/struct
extension DetailsVMDelegate {
    
    func feedback(data: String) {
        print("User Not Found")
    }
}
