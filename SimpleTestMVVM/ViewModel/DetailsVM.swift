//
//  DetailsVM.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 24/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import Foundation

struct DetailsVM: DetailsVMProtocol, DataService {
    
    var detailsCoordinator: DetailsCoordinator
    
    var detailsVMDelegate: DetailsVMDelegate?
    
    var user: User?
    
    init(coordinator: DetailsCoordinator) {
        self.detailsCoordinator = coordinator
    }
    
    func didCancel() {
        // Back to StartVC
        self.detailsCoordinator.transition(toScreen: Screen.main, withData: nil)
    }
    
    func didSave(user: User) {
        // Update Model
        updateUser(user: user)
        // Back to StartVC
        self.detailsCoordinator.transition(toScreen: Screen.main, withData: nil)
    }
    
    
}
