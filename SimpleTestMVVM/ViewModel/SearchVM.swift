//
//  SearchVM.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 23/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import Foundation

struct SearchVM: SearchVMProtocol, DataService {
    
    var searchCoordinator: SearchCoordinator
    
    // Can't insert 'weak' here because of struct
    var searchVMDelegate: SearchVMDelegate?
    
    init(coordinator: SearchCoordinator) {
        self.searchCoordinator = coordinator
    }
    
    // Network user check logic not yet implemented.
    func searchUser(userName: String) {
        
        if let user: User = checkUser(userName: userName) {
            self.searchVMDelegate?.feedback(data: "User Found !")
            self.searchCoordinator.transition(toScreen: Screen.detail, withData: user)
        }
        else {
            // Feedback delegate implemented by SearchVC
            self.searchVMDelegate?.feedback(data: "User Not Found")
        }
    }

}
