//
//  DetailsViewController.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 23/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {

    @IBOutlet var userName: UILabel!
    @IBOutlet var name: UITextField!
    @IBOutlet var surname: UITextField!
    @IBOutlet var birthdate: UITextField!
    
    var viewModel: DetailsVMProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Binding between ViewModel's model and View
        self.userName.text = self.viewModel?.user?.userName
        self.name.text = self.viewModel?.user?.name
        self.surname.text = self.viewModel?.user?.surname
        self.birthdate.text = self.viewModel?.user?.birthDate
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        self.viewModel?.didSave(user: (self.viewModel?.user)!)
    }
    
    
    @IBAction func cancelAction(_ sender: UIButton) {
        self.viewModel?.didCancel()
    }
}


extension DetailsVC: DetailsVMDelegate {
    
    func registerVMDelegate() {
        // SearchVC registers itself as a delegate of SearchVM, conforming to the SearchVMDelegate protocol.
        self.viewModel?.detailsVMDelegate = self
    }
    
    func feedback(data: String) {
        // SearchVM pass back data to SearchVC, that places the string into the feedback label.
        print("feedback")
    }
}
