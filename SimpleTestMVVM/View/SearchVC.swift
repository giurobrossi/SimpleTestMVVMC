//
//  ViewController.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 22/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {

    @IBOutlet var userName: UITextField!
    @IBOutlet var feedbackLabel: UILabel!
    
    // viewModel variable must be Optional because otherwise it should be initialized, but it wouldn't be possible since SearchVMProtocol is a Protocol
    var viewModel: SearchVMProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func searchUser(_ sender: Any) {
        self.registerVMDelegate()
        // Take note: viewModel hasn't been instantiated somewhere here, because it's a protocol. A concrete object has been assigned to viewModel externally, by the SearchCoordinator that started this VC. The magic here is possible because viewModel is a protocol !
        let vu = ViewControllerUtils()
        vu.showActivityIndicator(uiView: self.view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
            vu.hideActivityIndicator(uiView: self.view)
            self.viewModel?.searchUser(userName: self.userName.text!)
        })
    }
}

extension SearchVC: SearchVMDelegate {

    func registerVMDelegate() {
        // SearchVC registers itself as a delegate of SearchVM, conforming to the SearchVMDelegate protocol.
        self.viewModel?.searchVMDelegate = self
    }
    
    func feedback(data: String) {
        // SearchVM pass back data to SearchVC, that places the string into the feedback label.
        self.feedbackLabel.text = data
    }
}

