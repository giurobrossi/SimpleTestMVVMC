//
//  User.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 25/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import Foundation

struct User {
    private(set) var userName: String
    private(set) var name: String
    private(set) var surname: String
    private(set) var birthDate: String
    
    mutating func setUserName(userName: String) {
        self.userName = userName
    }
    
    mutating func setName(name: String) {
        self.name = name
    }
    
    mutating func setSurname(surname: String) {
        self.surname = surname
    }
    
    mutating func setBirthDate(birthDate: String) {
        self.birthDate = birthDate
    }
}
