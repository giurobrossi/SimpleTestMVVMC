//
//  pages.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 29/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import Foundation

enum Screen {
    case main, detail
}
