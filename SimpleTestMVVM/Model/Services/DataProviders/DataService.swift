//
//  DataService.swift
//  SimpleTestMVVM
//
//  Created by Giuseppe Roberto Rossi on 25/07/17.
//  Copyright © 2017 it.giuseppe. All rights reserved.
//

import Foundation

protocol DataService {
    
    func checkUser(userName: String) -> User?
    
    func updateUser(user: User)
}


extension DataService {
    
    func checkUser(userName: String) -> User? {
        
        if userName != "giuseppe" {
            return nil
        }
        else {
            let user = User(userName: userName, name: "Giuseppe", surname: "Rossi", birthDate: "03-06-1969")
            return user
        }
    }
    
    func updateUser(user: User) {
        print("user updated")
    }
}
